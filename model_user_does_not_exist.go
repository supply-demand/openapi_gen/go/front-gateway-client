/*
Front gateway

Front gateway

API version: 0.0.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// UserDoesNotExist struct for UserDoesNotExist
type UserDoesNotExist struct {
	Kind *string `json:"kind,omitempty"`
}

// NewUserDoesNotExist instantiates a new UserDoesNotExist object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUserDoesNotExist() *UserDoesNotExist {
	this := UserDoesNotExist{}
	var kind string = "userDoesNotExist"
	this.Kind = &kind
	return &this
}

// NewUserDoesNotExistWithDefaults instantiates a new UserDoesNotExist object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUserDoesNotExistWithDefaults() *UserDoesNotExist {
	this := UserDoesNotExist{}
	var kind string = "userDoesNotExist"
	this.Kind = &kind
	return &this
}

// GetKind returns the Kind field value if set, zero value otherwise.
func (o *UserDoesNotExist) GetKind() string {
	if o == nil || o.Kind == nil {
		var ret string
		return ret
	}
	return *o.Kind
}

// GetKindOk returns a tuple with the Kind field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UserDoesNotExist) GetKindOk() (*string, bool) {
	if o == nil || o.Kind == nil {
		return nil, false
	}
	return o.Kind, true
}

// HasKind returns a boolean if a field has been set.
func (o *UserDoesNotExist) HasKind() bool {
	if o != nil && o.Kind != nil {
		return true
	}

	return false
}

// SetKind gets a reference to the given string and assigns it to the Kind field.
func (o *UserDoesNotExist) SetKind(v string) {
	o.Kind = &v
}

func (o UserDoesNotExist) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Kind != nil {
		toSerialize["kind"] = o.Kind
	}
	return json.Marshal(toSerialize)
}

type NullableUserDoesNotExist struct {
	value *UserDoesNotExist
	isSet bool
}

func (v NullableUserDoesNotExist) Get() *UserDoesNotExist {
	return v.value
}

func (v *NullableUserDoesNotExist) Set(val *UserDoesNotExist) {
	v.value = val
	v.isSet = true
}

func (v NullableUserDoesNotExist) IsSet() bool {
	return v.isSet
}

func (v *NullableUserDoesNotExist) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUserDoesNotExist(val *UserDoesNotExist) *NullableUserDoesNotExist {
	return &NullableUserDoesNotExist{value: val, isSet: true}
}

func (v NullableUserDoesNotExist) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUserDoesNotExist) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


