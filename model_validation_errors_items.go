/*
Front gateway

Front gateway

API version: 0.0.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ValidationErrorsItems struct for ValidationErrorsItems
type ValidationErrorsItems struct {
	Key *string `json:"key,omitempty"`
	Rule *string `json:"rule,omitempty"`
	Message *string `json:"message,omitempty"`
}

// NewValidationErrorsItems instantiates a new ValidationErrorsItems object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewValidationErrorsItems() *ValidationErrorsItems {
	this := ValidationErrorsItems{}
	return &this
}

// NewValidationErrorsItemsWithDefaults instantiates a new ValidationErrorsItems object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewValidationErrorsItemsWithDefaults() *ValidationErrorsItems {
	this := ValidationErrorsItems{}
	return &this
}

// GetKey returns the Key field value if set, zero value otherwise.
func (o *ValidationErrorsItems) GetKey() string {
	if o == nil || o.Key == nil {
		var ret string
		return ret
	}
	return *o.Key
}

// GetKeyOk returns a tuple with the Key field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ValidationErrorsItems) GetKeyOk() (*string, bool) {
	if o == nil || o.Key == nil {
		return nil, false
	}
	return o.Key, true
}

// HasKey returns a boolean if a field has been set.
func (o *ValidationErrorsItems) HasKey() bool {
	if o != nil && o.Key != nil {
		return true
	}

	return false
}

// SetKey gets a reference to the given string and assigns it to the Key field.
func (o *ValidationErrorsItems) SetKey(v string) {
	o.Key = &v
}

// GetRule returns the Rule field value if set, zero value otherwise.
func (o *ValidationErrorsItems) GetRule() string {
	if o == nil || o.Rule == nil {
		var ret string
		return ret
	}
	return *o.Rule
}

// GetRuleOk returns a tuple with the Rule field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ValidationErrorsItems) GetRuleOk() (*string, bool) {
	if o == nil || o.Rule == nil {
		return nil, false
	}
	return o.Rule, true
}

// HasRule returns a boolean if a field has been set.
func (o *ValidationErrorsItems) HasRule() bool {
	if o != nil && o.Rule != nil {
		return true
	}

	return false
}

// SetRule gets a reference to the given string and assigns it to the Rule field.
func (o *ValidationErrorsItems) SetRule(v string) {
	o.Rule = &v
}

// GetMessage returns the Message field value if set, zero value otherwise.
func (o *ValidationErrorsItems) GetMessage() string {
	if o == nil || o.Message == nil {
		var ret string
		return ret
	}
	return *o.Message
}

// GetMessageOk returns a tuple with the Message field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ValidationErrorsItems) GetMessageOk() (*string, bool) {
	if o == nil || o.Message == nil {
		return nil, false
	}
	return o.Message, true
}

// HasMessage returns a boolean if a field has been set.
func (o *ValidationErrorsItems) HasMessage() bool {
	if o != nil && o.Message != nil {
		return true
	}

	return false
}

// SetMessage gets a reference to the given string and assigns it to the Message field.
func (o *ValidationErrorsItems) SetMessage(v string) {
	o.Message = &v
}

func (o ValidationErrorsItems) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Key != nil {
		toSerialize["key"] = o.Key
	}
	if o.Rule != nil {
		toSerialize["rule"] = o.Rule
	}
	if o.Message != nil {
		toSerialize["message"] = o.Message
	}
	return json.Marshal(toSerialize)
}

type NullableValidationErrorsItems struct {
	value *ValidationErrorsItems
	isSet bool
}

func (v NullableValidationErrorsItems) Get() *ValidationErrorsItems {
	return v.value
}

func (v *NullableValidationErrorsItems) Set(val *ValidationErrorsItems) {
	v.value = val
	v.isSet = true
}

func (v NullableValidationErrorsItems) IsSet() bool {
	return v.isSet
}

func (v *NullableValidationErrorsItems) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableValidationErrorsItems(val *ValidationErrorsItems) *NullableValidationErrorsItems {
	return &NullableValidationErrorsItems{value: val, isSet: true}
}

func (v NullableValidationErrorsItems) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableValidationErrorsItems) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


